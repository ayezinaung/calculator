//
//  ViewController.swift
//  Calculator
//
//  Created by AdventSoft on 16/6/19.
//  Copyright © 2019 hello. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var txtNum1: UITextField!
    @IBOutlet weak var txtNum2: UITextField!
    @IBOutlet weak var txtResult: UILabel!
    var Num1 = 0;
    var Num2 = 0;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        // git check out
        
        //change
    }

    
    
    @IBAction func btnMoud(_ sender: Any) {
        Num1 = (Int(txtNum1.text ?? "")) ?? 0;
        Num2 = (Int(txtNum2.text ?? "")) ?? 0;
        txtResult.text = "\(Num1 / Num2)" ;
        
    }
    @IBAction func btnMulti(_ sender: Any) {
        Num1 = (Int(txtNum1.text ?? "")) ?? 0;
        Num2 = (Int(txtNum2.text ?? "")) ?? 0;
        txtResult.text = "\(Num1 * Num2)" ;
        
    }
    @IBAction func btnAdd(_ sender: Any) {
        Num1 = (Int(txtNum1.text ?? "")) ?? 0;
        Num2 = (Int(txtNum2.text ?? "")) ?? 0;
        txtResult.text = "\(Num1 + Num2)" ;
    }
    
    @IBAction func btnMinus(_ sender: Any) {
        Num1 = (Int(txtNum1.text ?? "")) ?? 0;
        Num2 = (Int(txtNum2.text ?? "")) ?? 0;
        txtResult.text = "\(Num1 - Num2)" ;
    }
}

